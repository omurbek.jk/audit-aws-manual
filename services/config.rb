coreo_aws_rule "manual-full-inventory-performed" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-full-inventory-performed.html"
  display_name "Full Inventory Performed"
  description "Verify the full inventory CloudCoreo composite has been executed and the results have been evaluated"
  category "Security"
  suggested_action "You Responded with the MANUAL_FULL_INVENTORY_PERFORMED variable: ${MANUAL_FULL_INVENTORY_PERFORMED}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.1.20, 3.4.5"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-encryption-audit-performed" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-encryption-audit-performed.html"
  display_name "Encryption Audit Performed"
  description "Verify the audit of all encryptable objects has been performed"
  category "Security"
  suggested_action "You Responded with the MANUAL_ENCRYPTION_AUDIT_PERFORMED variable: ${MANUAL_ENCRYPTION_AUDIT_PERFORMED}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.13.11, 3.13.8"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-communication-session-authenticity" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-communication-session-authenticity.html"
  display_name "Communications Sessions Authenticity"
  description "Outline the controls implemented to protect session communications."
  category "Security"
  suggested_action "You Responded with the MANUAL_COMMUNICATION_SESSION_AUTHENTICITY_INPUT variable: ${MANUAL_COMMUNICATION_SESSION_AUTHENTICITY_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.13.15"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-remote-access-points-secured" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-remote-access-points-secured.html"
  display_name "Remote Access Points Secured"
  description "A manual rule to verify the organization has secured all of its accesspoints via technologies such as ssh or RDP (Remote Desktop Protocol)"
  category "Security"
  suggested_action "You Responded with the MANUAL_REMOTE_ACCESS_POINTS_SECURED variable: ${MANUAL_REMOTE_ACCESS_POINTS_SECURED}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.1.14"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-access-through-vpn" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-access-through-vpn.html"
  display_name "Remote access is only through VPN connections"
  description "Ensure remote access is through corporate VPN. The corporate VPN has a set username and password that the users cannot modify. VPN access is not monitored by the site. VPN provides access to entire site."
  category "Security"
  suggested_action "You Responded with the MANUAL_ACCESS_THROUGH_VPN_INPUT variable: ${MANUAL_ACCESS_THROUGH_VPN_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.1.15"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-mobile-device-guidelines" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-mobile-device-guidelines.html"
  display_name "Mobile device guidelines are established"
  description "Site officials should establish guidelines for the use of mobile devices and restrict the operation of those devices to the guidelines."
  category "Security"
  suggested_action "You Responded with the MANUAL_MOBILE_DEVICE_GUIDELINES_INPUT variable: ${MANUAL_MOBILE_DEVICE_GUIDELINES_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.1.18"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-mobile-device-cui-encryption" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-mobile-device-cui-encryption.html"
  display_name "CUI data encryption on mobile devices"
  description "All mobile devices should be encrypted."
  category "Security"
  suggested_action "You Responded with the MANUAL_MOBILE_DEVICE_CUI_ENCRYPTION_INPUT variable: ${MANUAL_MOBILE_DEVICE_CUI_ENCRYPTION_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.1.19"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-maintenance-records" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-maintenance-records.html"
  display_name "Ensure System Components Maintained to Best Practices"
  description "All maintenance activities should be both approved and monitored whether or on or off site"
  category "Security"
  suggested_action "You Responded with the MANUAL_MAINTENANCE_RECORDS_INPUT variable: ${MANUAL_MAINTENANCE_RECORDS_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.7.1, 3.7.2, 3.7.3"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-approved-monitored-maintenance" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-approved-monitored-maintenance.html"
  display_name "Ensure that all maintenance activities are approved and monitored"
  description "Ensure that all system components are repaired and those repairs documented and reviewed in line with organization/vendor/manufacturer specifications"
  category "Security"
  suggested_action "You Responded with the MANUAL_APPROVED_MONITORED_MAINTENANCE_INPUT variable: ${MANUAL_APPROVED_MONITORED_MAINTENANCE_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.7.1, 3.7.2, 3.7.3"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-component-removal-approval" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-component-removal-approval.html"
  display_name "Ensure Explicit Approval pre Component Removal"
  description "Ensure that the removal of any system or component from premsis for maintenance requires the explicit approval of a specified person/department."
  category "Security"
  suggested_action "You Responded with the MANUAL_COMPONENT_REMOVAL_APPROVAL_INPUT variable: ${MANUAL_COMPONENT_REMOVAL_APPROVAL_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.7.1, 3.7.2, 3.7.3"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-ensure-security-questions" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-ensure-security-questions.html"
  display_name "Ensure Account Security Questions"
  description "Security Questions improve account security"
  category "Security"
  suggested_action "You Responded with the MANUAL_ENSURE_SECURITY_QUESTIONS_INPUT variable: ${MANUAL_ENSURE_SECURITY_QUESTIONS_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "1.15"
  meta_cis_scored "false"
  meta_cis_level "1"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-obscure-auth-info" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-obscure-auth-info.html"
  display_name "Ensure Authorization Information is Obscured"
  description "Obscuring authorization information during authorization process improves security"
  category "Security"
  suggested_action "You Responded with the MANUAL_OBSCURE_AUTH_INFO_INPUT variable: ${MANUAL_OBSCURE_AUTH_INFO_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_nist_171_id "3.5.11"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-detailed-billing" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-detailed-billing.html"
  display_name "Enable Detailed Billing"
  description "Detailed billing can help to bring attention to anomalous use of AWS resources"
  category "Security"
  suggested_action "You Responded with the MANUAL_DETAILED_BILLING_INPUT variable: ${MANUAL_DETAILED_BILLING_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "1.17"
  meta_cis_scored "true"
  meta_cis_level "1"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "manual-strategic-iam-roles" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-strategic-iam-roles.html"
  display_name "Ensure Strategic IAM Roles"
  description "Use IAM Master and Manager Roles to optimize security"
  category "Security"
  suggested_action "You Responded with the MANUAL_STRATEGIC_IAM_ROLES_INPUT variable: ${MANUAL_STRATEGIC_IAM_ROLES_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "1.18"
  meta_cis_scored "true"
  meta_cis_level "1"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "manual-contact-details" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-contact-details.html"
  display_name "Maintain Contact Details"
  description "Contact details associated with the AWS account may be used by AWS staff to contact the account owner"
  category "Security"
  suggested_action "You Responded with the MANUAL_CONTACT_DETAILS_INPUT variable: ${MANUAL_CONTACT_DETAILS_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "1.19"
  meta_cis_scored "true"
  meta_cis_level "1"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-security-contact" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-security-contact.html"
  display_name "Security Contact Details"
  description "Contact details may be provided to the AWS account for your security team, allowing AWS staff to contact them when required"
  category "Security"
  suggested_action "You Responded with the MANUAL_SECURITY_CONTACT_INPUT variable: ${MANUAL_SECURITY_CONTACT_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "1.20"
  meta_cis_scored "true"
  meta_cis_level "1"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-resource-instance-access" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-resource-instance-access.html"
  display_name "IAM Instance Roles"
  description "Proper usage of IAM roles reduces the risk of active, unrotated keys"
  category "Security"
  suggested_action "You Responded with the MANUAL_RESOURCE_INSTANCE_ACCESS_INPUT variable: ${MANUAL_RESOURCE_INSTANCE_ACCESS_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "1.21"
  meta_cis_scored "false"
  meta_cis_level "2"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-appropriate-sns-subscribers" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-appropriate-sns-subscribers.html"
  display_name "SNS Appropriate Subscribers"
  description "Unintended SNS subscribers may pose a security risk"
  category "Security"
  suggested_action "You Responded with the MANUAL_APPROPRIATE_SNS_SUBSCRIBERS_INPUT variable: ${MANUAL_APPROPRIATE_SNS_SUBSCRIBERS_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "3.15"
  meta_cis_scored "false"
  meta_cis_level "1"
  meta_nist_171_id "3.4.3"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule "manual-least-access-routing-tables" do
  action :define
  service :user
  link "http://kb.cloudcoreo.com/mydoc_manual-least-access-routing-tables.html"
  display_name "Least Access Routing Tables"
  description "Being highly selective in peering routing tables minimizes impact of potential breach"
  category "Security"
  suggested_action "You Responded with the MANUAL_LEAST_ACCESS_ROUTING_TABLES_INPUT variable: ${MANUAL_LEAST_ACCESS_ROUTING_TABLES_INPUT}"
  level "High"
  meta_always_show_card "true"
  meta_cis_id "4.5"
  meta_cis_scored "false"
  meta_cis_level "2"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
end

coreo_aws_rule_runner "advise-manual" do
  service :ec2
  action :run
  regions ["PLAN::region"]
  rules []
end

coreo_uni_util_jsrunner "advise-manual" do
  action :run
  json_input '{}'
  data_type "json"
  packages([
               {
                   :name => "moment",
                   :version => "2.19.1"
               }
           ])
  function <<-RUBY

    function isValidDate(str) {
    
      var ret = moment(str, 'D/M/YYYY', true).isValid()
          || moment(str, 'DD/MM/YYYY', true).isValid()
          || moment(str, 'D/M/YY', true).isValid()
          || moment(str, 'DD/MM/YY', true).isValid()
          || moment(str, 'MM/DD/YYYY', true).isValid()
          || moment(str, 'M/D/YY', true).isValid()
          || moment(str, 'MM/DD/YY', true).isValid()
          || moment(str, 'M/D/YYYY', true).isValid();
      return ret;
    }

    function isDefault(str) {
        return (defaults.indexOf(str) > -1);
    }

    // this logic is negated
    if (isDefault('${MANUAL_MAINTENANCE_RECORDS_INPUT}') || '${MANUAL_MAINTENANCE_RECORDS_INPUT}'.toLowerCase().length                                < 5 ) { json_input = addViolation(json_input, 'manual-maintenance-records'); }
    if (isDefault('${MANUAL_APPROVED_MONITORED_MAINTENANCE_INPUT}') || '${MANUAL_APPROVED_MONITORED_MAINTENANCE_INPUT}'.toLowerCase().length          < 5 ) { json_input = addViolation(json_input, 'manual-approved-monitored-maintenance'); }
    if (isDefault('${MANUAL_COMPONENT_REMOVAL_APPROVAL_INPUT}') || '${MANUAL_COMPONENT_REMOVAL_APPROVAL_INPUT}'.toLowerCase().length                  < 5 ) { json_input = addViolation(json_input, 'manual-component-removal-approval'); }
    if (isDefault('${MANUAL_OBSCURE_AUTH_INFO_INPUT}') || '${MANUAL_OBSCURE_AUTH_INFO_INPUT}'.toLowerCase().length                                   < 10 ) { json_input = addViolation(json_input, 'manual-obscure-auth-info'); }
    if (isDefault('${MANUAL_COMMUNICATION_SESSION_AUTHENTICITY_INPUT}') || '${MANUAL_COMMUNICATION_SESSION_AUTHENTICITY_INPUT}'.toLowerCase().length < 10 ) { json_input = addViolation(json_input, 'manual-communication-session-authenticity'); }
    if (isDefault('${MANUAL_ACCESS_THROUGH_VPN_INPUT}') || '${MANUAL_ACCESS_THROUGH_VPN_INPUT}'.toLowerCase()                                       !== '') { json_input = addViolation(json_input, 'manual-access-through-vpn'); }
    if (isDefault('${MANUAL_MOBILE_DEVICE_GUIDELINES_INPUT}') || '${MANUAL_MOBILE_DEVICE_GUIDELINES_INPUT}'.toLowerCase().length                      < 5 ) { json_input = addViolation(json_input, 'manual-mobile-device-guidelines'); }
    if (isDefault('${MANUAL_MOBILE_DEVICE_CUI_ENCRYPTION_INPUT}') || '${MANUAL_MOBILE_DEVICE_CUI_ENCRYPTION_INPUT}'.toLowerCase()                   !== '') { json_input = addViolation(json_input, 'manual-mobile-device-cui-encryption'); }
    if (!isValidDate('${MANUAL_REMOTE_ACCESS_POINTS_SECURED}')                                                                                           ) { json_input = addViolation(json_input, 'manual-remote-access-points-secured'); }
    if (!isValidDate('${MANUAL_FULL_INVENTORY_PERFORMED}')                                                                                               ) { json_input = addViolation(json_input, 'manual-full-inventory-performed'); }
    if (isDefault('${MANUAL_ENCRYPTION_AUDIT_PERFORMED}') || '${MANUAL_ENCRYPTION_AUDIT_PERFORMED}'.toLowerCase().length                             < 10 ) { json_input = addViolation(json_input, 'manual-encryption-audit-performed'); }
    //if ('${MANUAL_ENSURE_SECURITY_QUESTIONS_INPUT}'.toLowerCase()                !== '') { json_input = addViolation(json_input, 'manual-ensure-security-questions'); }
    //if ('${MANUAL_DETAILED_BILLING_INPUT}'.toLowerCase()                         !== '') { json_input = addViolation(json_input, 'manual-detailed-billing'); }
    //if ('${MANUAL_STRATEGIC_IAM_ROLES_INPUT}'.toLowerCase()                      !== '') { json_input = addViolation(json_input, 'manual-strategic-iam-roles'); }
    //if ('${MANUAL_CONTACT_DETAILS_INPUT}'.toLowerCase()                          !== '') { json_input = addViolation(json_input, 'manual-contact-details'); }
    //if ('${MANUAL_SECURITY_CONTACT_INPUT}'.toLowerCase()                         !== '') { json_input = addViolation(json_input, 'manual-security-contact'); }
    //if ('${MANUAL_RESOURCE_INSTANCE_ACCESS_INPUT}'.toLowerCase()                 !== '') { json_input = addViolation(json_input, 'manual-resource-instance-access'); }
    //if ('${MANUAL_APPROPRIATE_SNS_SUBSCRIBERS_INPUT}'.toLowerCase()              !== '') { json_input = addViolation(json_input, 'manual-appropriate-sns-subscribers'); }
    //if ('${MANUAL_LEAST_ACCESS_ROUTING_TABLES_INPUT}'.toLowerCase()              !== '') { json_input = addViolation(json_input, 'manual-least-access-routing-tables'); }

    var return_value = json_input['violations'];
    if( return_value === undefined ) {
        return_value = {};
    }
    callback(return_value);
}

const moment = require('moment');

const defaults = [
        "Document CUI EC2 instances and encryption used",
        "Enter the date of the audit-aws-inventory-all list review",
        "Enter the date of audit",
        "Document approval process. Enter policy title.",
        "Document approval process. Enter policy title.",
        "Document approval process. Enter policy title.",
        "Document user access method to AWS",
        "Verify and enter the SNS topics verified",
        "Document access to AWS environment",
        "Specify mobile device policy"
    ];

const ruleMetaJSON = {
     'manual-maintenance-records': COMPOSITE::coreo_aws_rule.manual-maintenance-records.inputs,
     'manual-approved-monitored-maintenance': COMPOSITE::coreo_aws_rule.manual-approved-monitored-maintenance.inputs,
     'manual-component-removal-approval': COMPOSITE::coreo_aws_rule.manual-component-removal-approval.inputs,
     'manual-ensure-security-questions': COMPOSITE::coreo_aws_rule.manual-ensure-security-questions.inputs,
     'manual-obscure-auth-info': COMPOSITE::coreo_aws_rule.manual-obscure-auth-info.inputs,
     'manual-detailed-billing': COMPOSITE::coreo_aws_rule.manual-detailed-billing.inputs,
     'manual-strategic-iam-roles': COMPOSITE::coreo_aws_rule.manual-strategic-iam-roles.inputs,
     'manual-contact-details': COMPOSITE::coreo_aws_rule.manual-contact-details.inputs,
     'manual-security-contact': COMPOSITE::coreo_aws_rule.manual-security-contact.inputs,
     'manual-resource-instance-access': COMPOSITE::coreo_aws_rule.manual-resource-instance-access.inputs,
     'manual-appropriate-sns-subscribers': COMPOSITE::coreo_aws_rule.manual-appropriate-sns-subscribers.inputs,
     'manual-least-access-routing-tables': COMPOSITE::coreo_aws_rule.manual-least-access-routing-tables.inputs,
     'manual-mobile-device-cui-encryption': COMPOSITE::coreo_aws_rule.manual-mobile-device-cui-encryption.inputs,
     'manual-mobile-device-guidelines': COMPOSITE::coreo_aws_rule.manual-mobile-device-guidelines.inputs,
     'manual-access-through-vpn': COMPOSITE::coreo_aws_rule.manual-access-through-vpn.inputs,
     'manual-remote-access-points-secured': COMPOSITE::coreo_aws_rule.manual-remote-access-points-secured.inputs,
     'manual-communication-session-authenticity': COMPOSITE::coreo_aws_rule.manual-communication-session-authenticity.inputs,
     'manual-full-inventory-performed': COMPOSITE::coreo_aws_rule.manual-full-inventory-performed.inputs,
     'manual-encryption-audit-performed': COMPOSITE::coreo_aws_rule.manual-encryption-audit-performed.inputs
 };

const ruleInputsToKeep = ['service', 'category', 'link', 'display_name', 'suggested_action', 'description', 'level', 'meta_cis_id', 'meta_cis_scored', 'meta_cis_level', 'include_violations_in_count', 'meta_nist_171_id'];
const ruleMeta = {};

Object.keys(ruleMetaJSON).forEach(rule => {
    const flattenedRule = {};
    ruleMetaJSON[rule].forEach(input => {
        if (ruleInputsToKeep.includes(input.name))
            flattenedRule[input.name] = input.value;
    })
    flattenedRule["service"] = "iam";
    ruleMeta[rule] = flattenedRule;
});

function addViolation(json_input, rule_name) {
    if(${AUDIT_AWS_MANUAL_ALERT_LIST}.indexOf(rule_name) == -1) { return json_input; }
    if (!json_input['violations']) {
        json_input['violations'] = {};
    }
    if (!json_input['violations']['us-east-1']) {
        json_input['violations']['us-east-1'] = {};
    }
    if (!json_input['violations']['us-east-1']['customer']) {
        json_input['violations']['us-east-1']['customer'] = {};
    }
    if (!json_input['violations']['us-east-1']['customer']['violating_info']) {
        json_input['violations']['us-east-1']['customer']['violating_info'] = {};
    }
    if (!json_input['violations']['us-east-1']['customer']['violations']) {
        json_input['violations']['us-east-1']['customer']['violations'] = {}
    }
    json_input['violations']['us-east-1']['customer']['violator_info'] = { 'name': 'customer' };
    json_input['violations']['us-east-1']['customer']['violations'][rule_name] = Object.assign(ruleMeta[rule_name]);

    return json_input;

RUBY
end

coreo_uni_util_variables "advise-manual" do
  action :set
  variables([
                {'COMPOSITE::coreo_aws_rule_runner.advise-manual.report' => 'COMPOSITE::coreo_uni_util_jsrunner.advise-manual.return'}
            ])
end
